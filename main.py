import os
import pwd
import stat
import glob
import platform
import subprocess

file = open('/root/report', 'w')
# The function of checking file access rules
def get_file_permissions(filename):
    if os.path.exists(filename):
        file_stat = os.stat(filename)
        permissions = file_stat.st_mode
        owner_permissions = stat.S_IMODE(permissions)
        file_owner = file_stat.st_uid
        owner = pwd.getpwuid(file_owner).pw_name
        return {'permissions': oct(owner_permissions), 'owner': owner}
    else:
        return 'error'


# Checking the kernel version for CVE
def is_kernel_vulnerable(kernel_version, cve):
    kernel = (int((kernel_version.split("-")[0].split("."))[0]), int((kernel_version.split("-")[0].split("."))[1]), int((kernel_version.split("-")[0].split("."))[2]))
    if cve == 'dirty pipe':
        if kernel[0] == 5:
            if kernel[1] < 8:
                return False
            if kernel[1] == 10 and kernel[2] >= 102:
                return False
            if kernel[1] == 15 and kernel[2] >= 25:
                return False
            if kernel[1] >= 16 and kernel[2] >= 11:
                return False
            return True
        else:
            return False
    elif cve == 'dirty cow':
        if kernel[0] == 4:
            if kernel[1] < 8:
                return True
            elif kernel[1] == 8:
                if kernel[2] <= 3:
                    return True
                else:
                    return False
            else:
                return True
        elif 2 < kernel[0] < 4:
            return True
        else:
            return False
    else:
        file.write('Check the kernel version. The analyzer could not verify the version.\n')


# Creating a list of all non-system users of the system

users = ['root']

if os.path.exists('/etc/passwd'):
    with open('/etc/passwd', 'r') as file1:
        for line in file1.readlines():
            loc = line.split(":")
            if 1000 <= int(loc[2]) <= 60000:
                users.append(loc[0])
else:
    file.write('The /etc/passwd file is not readable or not exist.\n')
file.write("Пользователи системы: " + '; '.join(users) + '\n')

# The root's file analysis block

root_files = ['/etc/shadow', '/root/.ssh'] + glob.glob('/root/.ssh/*')
for current_file in root_files:
    file_info = get_file_permissions(current_file)
    if file_info != 'error':
        if file_info['permissions'] == oct(384) and file_info['owner'] == 'root':
            file.write(f'The {current_file} file is valid.\n')
        else:
            if file_info['permissions'] != oct(384):
                file.write(f'You have incorrect access rights to the {current_file} file. You have a {file_info["permissions"]}, but it should be "0o600".\n')
            if file_info['owner'] != 'root':
                file.write(f'You have the owner of the file {current_file}. You have a {file_info["owner"]}, but there should be a root.\n')
    else:
        file.write(f'The {current_file} file is not readable or not exist.\n')

# The user's file analysis block
for user in users:
    for current_file in glob.glob('/home/'+ user +'/.ssh/*'):
        file_info = get_file_permissions(current_file)
        if file_info != 'error':
            if file_info['permissions'] == oct(384) and file_info['owner'] == user:
                file.write(f'The {current_file} file is valid.\n')
            else:
                if file_info['permissions'] != oct(384):
                    file.write(
                        f'You have incorrect access rights to the {current_file} file. You have a {file_info["permissions"]}, but it should be "0o600".\n')
                if file_info['owner'] != user:
                    file.write(
                        f'You have the owner of the file {current_file}. You have a {file_info["owner"]}, but there should be a {user}.\n')
        else:
            file.write(f'The {current_file} file is not readable or not exist.\n')


# Checking which SUDO files have the SUID bit set

file.write("\n\n Checking which SUDO files have the SUID bit set.\n")
for root, dirs, files in os.walk('/'):
    for file1 in files:
        full_path = os.path.join(root, file1)
        if os.access(full_path, os.X_OK) and os.path.isfile(full_path) and os.stat(full_path).st_uid == 0:
            if os.stat(full_path).st_mode & 0o4000:
                file.write(f"WARNING. The file {full_path} has the SUID bit set.\n")


# Checking which Home's files have the SUID bit set

file.write("\n\nChecking which Home's files have the SUID bit set.\n")

for current_file in glob.glob('/home/**', recursive=True):
    if os.stat(current_file).st_mode & 0o4000:
        file.write(f"WARNING. The file {current_file} has the SUID bit set.\n")

# Search for files with unsafe capability

file.write('\n\n\nFiles with unsafe capability:\n')
for root, dirs, files in os.walk('/usr/bin'):
    for file1 in files:
        file_path = os.path.join(root, file1)
        if os.popen(f"getcap {file_path} 2>/dev/null").read().strip():
            file.write(file_path + '\n')

# Checking the system for CVE

file.write('\n\nThe system is vulnerable to the PwnKit vulnerability (CVE-2021-4034) because you have the pkexec package in your system.\n' if os.popen('which pkexec').read().strip() else 'The system is not exposed to the PwnKit vulnerability (CVE-2021-4034)\n')
file.write('The system is vulnerable to vulnerability CVE-2022-0847. Update kernel.\n' if is_kernel_vulnerable(platform.release(), 'dirty pipe') else 'The system is not vulnerable to vulnerability CVE-2022-0847.\n')
file.write('The system is vulnerable to vulnerability CVE-2016-5195. Update kernel.\n' if is_kernel_vulnerable(platform.release(), 'dirty cow') else 'The system is not vulnerable to vulnerability CVE-2016-5195.\n')

# Analyzing the /etc/ssh/sshd_config file for unsafe directives

file.write('\n\nAnalyzing the /etc/ssh/sshd_config file for unsafe directives.\n')

ssh_config = subprocess.run('sshd -T', shell=True, capture_output=True, text=True).stdout.replace(" ", "")
if ssh_config:
    if "port22" in ssh_config:
        file.write('You are using the default port for the ssh service. Use any unused port except 22.\n')
    if "permitrootloginyes" in ssh_config:
        file.write('Direct ssh access to the root account is prohibited. Block access to the account and leave access only to the regular account.\n')
    if "passwordauthenticationyes" in ssh_config:
        file.write("For security reasons, using a password to log in is not the best practice. Use the key input.\n")
    if "permitemptypasswordsyes" in ssh_config:
        file.write('Allowing empty passwords. This can leave the system vulnerable to attacks.\n')
    if "x11forwardingyes" in ssh_config:
        file.write("Enabling the X11 Forwarding feature allows redirection of the X11 GUI via SSH. If not in use, it is recommended to disable it to reduce the attack surface.\n")
    if "allowtcpforwardingyes" in ssh_config:
        file.write('If the "AllowTcpForwarding" is set to "yes", this allows TCP traffic to be forwarded over SSH, which can be used by attackers to bypass the firewall.\n')
    if "pubkeyauthenticationno" in ssh_config:
        file.write('Pubkey Authentication is necessary to use public key authentication. It is disabled in the system.\n')
    if "ignorerhostsno" in ssh_config:
        file.write('Ignore Rhosts set to "no", this allows the use of .rhosts files for authentication, which can create a vulnerability for session hijacking attacks.\n')
    if "allowagentforwardingyes" in ssh_config:
        file.write('If the "Allow Agent Forwarding" is set to "yes", this allows you to transfer agent data via SSH, which may pose a security threat. It is recommended to disable this option if it is not needed.\n')
else:
    file.write('The /etc/ssh/sshd_config file is not readable or not exist')

file.close()

